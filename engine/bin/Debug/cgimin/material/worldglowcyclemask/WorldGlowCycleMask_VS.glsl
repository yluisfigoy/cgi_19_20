#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;
in vec3 in_normal; 
in vec2 in_uv; 

// "modelview_projection_matrix" wird als Parameter erwartet, vom Typ Matrix4
uniform mat4 modelview_projection_matrix;
uniform mat4 model_view_matrix;

uniform float update;

// "texcoord" wird an den Fragment-Shader weitergegeben, daher als "out" deklariert
out vec2 texcoord;
out vec2 texcoord2;

out vec4 viewPosition;

void main()
{
	// "in_uv" (Texturkoordinate) wird direkt an den Fragment-Shader weitergereicht
	texcoord2 = in_uv;
	texcoord2.x = texcoord2.x + 0.5;

	texcoord = in_uv;
	texcoord.y = texcoord.y + update;

	// in gl_Position die finalan Vertex-Position geschrieben ("modelview_projection_matrix" * "in_position")
	gl_Position = modelview_projection_matrix * vec4(in_position, 1);
	viewPosition = model_view_matrix * vec4(in_position, 1.0);
}


